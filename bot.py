#!/usr/bin/env python
"""
Signal Bot for testing.
"""
import re
import json
import sqlite3
import random
import time
import logging

from semaphore import Bot, ChatContext


usage = """Usage:
@bot add|remove emoji word - add or remove an emoji to word mapping
@bot list - list all mappings for this channel
@bot help - display this help message
@bot source - link to bot's source code
"""

logging.basicConfig(level=logging.DEBUG)

with open('settings.json') as f:
    settings = json.load(f)

bot = Bot(settings['account'])

def select_group_emoji(message: str, group: str) -> str:
    options = {}
    con = sqlite3.connect('emoji.db')
    cur = con.cursor()
    for word in message.split():
        word = get_search_word(word)
        if word is None:
            continue
        cur.execute("SELECT emoji FROM group_tags WHERE tag = ? AND groupid = ?", [word, group])
        for r in cur:
            emoji = r[0]
            if emoji not in options:
                options[emoji] = 0
            options[emoji] += 1
    if len(options) == 0:
        return None
    emojis = []
    count = 0
    for e, c in options.items():
        if c > count:
            emojis = [e]
            count = c
        elif c == count:
            emojis.append(e)
    return random.choice(emojis)

def select_emoji(message: str) -> str:
    options = {}
    con = sqlite3.connect('emoji.db')
    cur = con.cursor()
    for word in message.split():
        word = get_search_word(word)
        if word is None:
            continue
        cur.execute("SELECT emoji FROM emoji_tags WHERE tag = ?", [word])
        for r in cur:
            emoji = r[0]
            if emoji not in options:
                options[emoji] = 0
            options[emoji] += 1
    if len(options) == 0:
        logging.info(f"no options for message, picking randomly")
        cur.execute("SELECT emoji FROM emoji_tags ORDER BY RANDOM() LIMIT 1")
        return cur.fetchone()[0]
    # logging.debug(f"options for message {message}: {options}")
    emojis = []
    count = 0
    for e, c in options.items():
        if c > count:
            emojis = [e]
            count = c
        elif c == count:
            emojis.append(e)
    return random.choice(emojis)

def get_search_word(original: str) -> str:
    word = ''.join(c for c in original if c.isalnum())
    if word.endswith("s"):
        word = word[:-1]
    elif word.endswith("ing"):
        word = word[:-3]
    if word in ["", "i"]:
        return None
    # if word != original:
    #     logging.debug(f"converting {original} into {word} for searching")
    return word

async def handle_mention(ctx: ChatContext) -> None:
    req = [a.strip() for a in ctx.message.get_body().split(" ") if len(a.strip()) > 0]

    con = sqlite3.connect('emoji.db')
    cur = con.cursor()

    group = ctx.message.data_message.groupV2.group_id
    if len(req) < 2:
        await ctx.message.reply(f"unsupported! {usage}")
        return

    if req[1] == "list":
        results = {}
        for row in cur.execute("SELECT emoji, tag FROM group_tags WHERE groupid = ?", [group]):
            if row[0] not in results:
                results[row[0]] = []
            results[row[0]].append(row[1])
        replymsg = ""
        for emoji, tags in results.items():
            if replymsg != "":
                replymsg += "\n"
            replymsg += f"{emoji}\n"
            for tag in tags:
                replymsg += f" • {tag}\n"
        if replymsg != "":
            await ctx.message.reply(body="👍", reaction=True)
            await ctx.message.reply(replymsg)
        else:
            await ctx.message.reply(body="❌", reaction=True)
            await ctx.message.reply(f"no custom reacts for this group! {usage}")
        return

    if req[1] == "help":
        await ctx.message.reply(usage)
        return

    if req[1] == "source":
        await ctx.message.reply(f"the source code for this bot can be found at {settings.get('source', 'https://gitlab.com/thefinn93/react-bot')}")
        return

    if len(req) != 4:
        await ctx.message.reply(f"unsupported! {usage}")
        return

    action = req[1]
    emoji = req[2]
    word = get_search_word(req[3])

    if word is None:
        ctx.message.reply(body="💁", reaction=True)
        await ctx.message.reply(f"{req[3]} doesn't look like a real word to me (would be converted to an empty string before entry)")
        return
    if action == "add":
        cur.execute("INSERT INTO group_tags (emoji, tag, groupid) VALUES (?, ?, ?)", [emoji, word, group])
    elif action == "delete" or action == "remove":
        cur.execute("DELETE FROM group_tags WHERE emoji = ? AND tag = ? AND groupid = ?", [emoji, word, group])
    else:
        await ctx.message.reply(body="💁", reaction=True)
        await ctx.message.reply(f'unsupported action {action}')
        return
    con.commit()
    await ctx.message.reply(body="👍", reaction=True)


@bot.handler('')
async def react(ctx: ChatContext) -> None:
    body = ctx.message.get_body().lower()
    if len(body) == 0 and len(ctx.message.data_message.attachments) == 0:
        return

    if len(ctx.message.data_message.mentions) > 0:
        mention = ctx.message.data_message.mentions[0]
        if mention.start == 0 and mention.uuid == settings['account_uuid']:
            await handle_mention(ctx)
            return

    if body.startswith("echo"):
        await ctx.message.reply(body=ctx.message.get_body())
    elif body.startswith("react ") and len(body) > 6:
        await ctx.message.reply(body=ctx.message.get_body()[6:], reaction=True)
    else:
        reaction = None
        if "good bot" in body:
            reaction = "❤️"
        elif ctx.message.data_message.groupV2 is not None:
            reaction = select_group_emoji(body, ctx.message.data_message.groupV2.group_id) or select_emoji(body)
        else:
            reaction = select_emoji(body)
        ctx.job_queue.log.info("Reacting with {}".format(reaction))
        await ctx.message.reply(body=reaction, reaction=True)

async def main():
    async with bot:
        await bot.start()

if __name__ == '__main__':
    import anyio
    anyio.run(main)