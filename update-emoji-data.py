#!/usr/bin/env python3
import urllib.request
import json
import sqlite3

# TODO: also do other dialects like _AU and _UK or maybe do all languages
SOURCE_DATA_URL = "https://github.com/signalapp/emoji-search-index/raw/main/data/en.json"


con = sqlite3.connect('emoji.db')
cur = con.cursor()

def get_search_word(word: str) -> str:
    word = ''.join(c for c in word if c.isalnum())
    if word.endswith("s"):
        return word[:-1]
    if word.endswith("ing"):
        return word[:-3]
    return word

cur.execute('CREATE TABLE IF NOT EXISTS group_tags (emoji TEXT NOT NULL, tag TEXT NOT NULL, groupid TEXT NOT NULL)')
cur.execute('CREATE TABLE IF NOT EXISTS emoji_tags (emoji TEXT NOT NULL, tag TEXT NOT NULL)')

# clear all existing entries from emoji_tags
cur.execute('DELETE FROM emoji_tags')

for row in json.load(urllib.request.urlopen(SOURCE_DATA_URL)):
    cur.executemany("insert into emoji_tags (emoji, tag) values (?, ?)", [(row['emoji'], get_search_word(tag)) for tag in row['tags']])

with open("settings.json") as f:
    settings = json.load(f)
    for emoji, tags in settings.get("custom", {}).get("add", []).items():
        cur.executemany("insert into emoji_tags (tag, emoji) values (?, ?)", [(get_search_word(tag), emoji) for tag in tags])

    for emoji, tags in settings.get("custom", {}).get("remove", []).items():
        cur.executemany("delete from emoji_tags where tag = ? and emoji = ?", [(get_search_word(tag), emoji) for tag in tags])

con.commit()