# React Bot

*a Signal bot that will react to every message sent to it*

this code is running on `+18446643466` on Signal

React Bot is a python 3 bot. It depends on the [semaphore](https://github.com/lwesterhof/semaphore) bot library,
and [signald](https://signald.org/).

Before running the bot:
* Configure signald with the Signal account you want the bot to use (`signaldctl account link` or `signaldctl account register`)
* Copy `settings.sample.json` to `settings.json` and edit it to have the phone number and UUID of your bot.
  * Add any custom emoji word associations as well
* Update the local emoji database: `python update-emoji-data.py`
  * this downloads the emoji database from [Signal's GitHub](https://github.com/SignalApp/emoji-search-index) and puts it and all customizations in `settings.json` into the sqlite database.

Finally, run the bot:

```
python bot.py
```